package main

import "fmt"

func main() {
	/*

					fmt.Println("Hello, world!")

					var x []int
					fmt.Println(x, len(x), cap(x))
					x = append(x, 10)
					fmt.Println(x, len(x), cap(x))
					x = append(x, 20)
					fmt.Println(x, len(x), cap(x))
					x = append(x, 30)
					fmt.Println(x, len(x), cap(x))
					x = append(x, 40)
					fmt.Println(x, len(x), cap(x))
					x = append(x, 50)
					fmt.Println(x, len(x), cap(x))

					var s string = "Hello there"
					var b byte = s[6]
					fmt.Println(b)

					x := []int{1, 2, 3, 4}
					d := [4]int{5, 6, 7, 8}
					y := make([]int, 2) // capity only has 2
					copy(y, d[:])       // because y only has 2 capacity, only copys first two elements of d, even though we are inputting all values of d.
					fmt.Println(y)
					copy(d[:], x)
					fmt.Println(d)

					var s string = "Hello there"
					var b byte = s[6]

					fmt.Println(b)

					var s string = "Hello there"
					var s2 string = s[4:7] // takes char at index 4 and DOES NOT TAKE char at index 7
					fmt.Println(s2)

					var s3 string = s[:5]
					fmt.Println(s3)

					var s4 string = s[6:]
					fmt.Println(s4)

					var s string = "Hello"
					var toBytes = []byte(s)
					fmt.Println(toBytes)

					var toRunes = []rune(s)
					fmt.Println(toRunes)

				// MAPS

				var nilMap map[string]int
				fmt.Println(nilMap)

				totalWins := map[string]int{}
				fmt.Println(totalWins)

				teams := map[string][]string{
					"Orcas":   []string{"Fred", "Ralph", "Bijou"},
					"Lions":   []string{"Sarah", "Peter", "Billie"},
					"Kittens": []string{"Waldo", "Raul", "Ze"},
				}

				fmt.Println(teams)

				// map with default size 10, but can grow over 10
				ages := make(map[int][]string, 10)
				fmt.Println(ages)


			totalWins := map[string]int{}
			totalWins["Orcas"] = 1
			totalWins["Lions"] = 2

			fmt.Println(totalWins["Orcas"])
			fmt.Println(totalWins["Kittens"]) // kittens was never set, so it returns the zero value of int
			totalWins["Kitten"]++
			fmt.Println(totalWins["Kitten"])
			totalWins["Lions"] = 3
			fmt.Println(totalWins["Lions"])


		// v ok idiom
		m := map[string]int{
			"hello": 5,
			"world": 0,
		}
		v, ok := m["hello"]
		fmt.Println(v, ok)

		v, ok = m["world"]
		fmt.Println(v, ok)

		v, ok = m["goodbye"]
		fmt.Println(v, ok) // ok will return false because it is not in the map

	*/
	// deleting from maps
	m := map[string]int{
		"hello": 5,
		"world": 10,
	}
	delete(m, "hello")
	fmt.Println(m)

	// emulating a set using golang map
	intSet := map[int]bool{}
	vals := []int{5, 10, 2, 5, 8, 7, 3, 9, 1, 2, 10}
	for _, v := range vals {
		intSet[v] = true
	}
	fmt.Println(len(vals), len(intSet))
	fmt.Println(intSet[5])
	fmt.Println(intSet[500])
	if intSet[100] {
		fmt.Println("100 is in the set")
	}
}
