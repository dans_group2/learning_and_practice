package main

import (
	"fmt"
)

func main() {
	fmt.Println("hello")

	type person struct {
		name string
		age  int
		pet  string
	}

	var fred person
	fmt.Println(fred) // empty struct consists of "zero values" of each field

	// struct literal, separated by commas and ordering aligns with struct defintion
	bob := person{
		"Bob",
		40,
		"cat",
	}
	fmt.Println(bob)

	// this struct literal does not require all fields to be declared
	beth := person{
		age:  30,
		name: "Beth",
	}

	fmt.Println(beth)

	// reading and writing to struct fields
	bob.name = "Bob Lee"
	fmt.Println(beth.name)

	// anonymous structs
	var anon struct {
		name string
		age  int
		pet  string
	}

	anon.name = "bob"
	anon.age = 50
	anon.pet = "dog"

	pet := struct {
		name string
		kind string
	}{
		name: "Fido",
		kind: "dog",
	}

	fmt.Println(pet)
}
