package main

import (
	"errors"
	"fmt"
)

func main() {
	// fmt.Println(div(1, 2))
	MyFunc(MyFuncOpts{
		FirstName: "Dan",
		Age:       11,
	})

	fmt.Println(withError(true))
	fmt.Println(withError(false))
}

func div(numerator int, denominator int) int {
	if denominator == 0 {
		return 0
	}

	return numerator / denominator
}

type MyFuncOpts struct {
	FirstName string
	LastName  string
	Age       int
}

func MyFunc(opts MyFuncOpts) {
	fmt.Println(opts)
}

func withError(triggerError bool) error {
	if triggerError {
		return errors.New("error triggered")
	}

	return nil
}
