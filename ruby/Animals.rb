require './Person'

class Pet
    attr_accessor :name, :age, :gender, :color

end

class Cat < Pet

    def initialize()
        @name = 'default'
    end


end

class Dog < Pet
    def bark(i)
        i.times do
            puts "Woof!"
        end
    end
end

class Snake < Pet
    attr_accessor :length
end


# snake = Snake.new
# snake.name = "Sammy"
# snake.length = 500

# pp snake.inspect

my_dog = Dog.new
my_dog.bark(2)

# 5.times do
#     my_dog.bark(3)
# end

# 5.times do |number|
#     puts number
#     my_dog.bark(3)
# end

x = "Test"
y = "String"

puts "Success!" if x + y == "TestString"

x = %q{this is a Test
of the multi
line
feature
}
pp x

class First
    class Second
        attr_accessor :second_field
    end
end

pp First::Second.new.inspect

pp Person.new