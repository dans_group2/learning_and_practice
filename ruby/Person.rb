class Person
    attr_accessor :name, :age, :gender

end



pp Person.new

person = Person.new
person.name = 'hello'

person.age = 2
person.gender = 'undefined'
pp person.inspect

class Person
    attr_accessor :new_name
    @private = 'maybe'
end

person = Person.new
person.new_name = 'hfsdf'
pp person.inspect
# person.private = 'changed'
# pp person.inspect
