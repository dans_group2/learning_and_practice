'''
Is Unique: Implement an algorithm to determine if a string has all unique characters. What if you
cannot use additional data structures
'''

# runtime O(n) where n is the length of the inputted string
# spacetime O(n) where n is the amount of unique characters in the inputted string
# algorithm explanation:
#   create a dictionary whose key is a letter of the inputted string and value is the count of how many times that letter shows up
#   loop through each letter in the inputted string
#   this loop will look for duplicates
#   for each letter
#       track the count of how many times that letter appears in the word
#       if the count is more than 1
#           return False out of the loop because we found a non unique letter
#   if the for loop completes without returning False
#       return True since the for loop failed to find duplicates.
from typing import Dict


def is_unique(string: str) -> bool:
    characterCount: Dict[str, int] = {}

    for letter in string:
        if letter in characterCount:
            characterCount[letter] += 1
            if characterCount[letter] > 1:
                return False
        else:
            characterCount[letter] = 1
    
    return True



import unittest


class TestIsUnique(unittest.TestCase):
    def test_empty_string(self):
        self.assertTrue(is_unique(""))

    def test_string_with_one_letter(self):
        self.assertTrue(is_unique("a"))

    def test_string_with_duplicates(self):
        self.assertFalse(is_unique("hello"))

    def test_string_without_duplicates(self):
        self.assertTrue(is_unique("abcdef"))


if __name__ == '__main__':
    unittest.main()