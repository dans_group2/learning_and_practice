'''
Check Permutation: Given two strings, write a method to decide if one is a permutation of the
other. 
'''

from typing import Dict


# runtime O(n) where n = length of string
# spacetime O(n) where n = number of characters in string
# algorithm explanation:
#   create a dictionary whose 
def check_permutation(str1: str, str2: str) -> bool:
    if len(str1) != len(str2):
        # if different lengths, cannot be a permutation
        return False
    
    letterCount : Dict[str, int] = {}

    for letter in str1:
        if letter not in letterCount:
            letterCount[letter] = 1
        else:
            letterCount[letter] += 1
    
    for letter in str2:
        if letter not in letterCount:
            return False
        
        if letterCount[letter] < 1:
            return False
        
        letterCount[letter] -= 1
    
    return True


import unittest

class TestCheckPermutation(unittest.TestCase):
    def test_check_different_length_strings(self):
        str1 = "hello"
        str2 = "goodbye"
        self.assertFalse(check_permutation(str1, str2))

    def test_valid_permutation_1(self):
        str1 = "a"
        str2 = "a"
        self.assertTrue(check_permutation(str1, str2))
    
    def test_false_permutation_1(self):
        str1 = "a"
        str2 = "b"
        self.assertFalse(check_permutation(str1, str2))

    def test_valid_permutation_2(self):
        str1 = "abc"
        str2 = "acb"
        self.assertTrue(check_permutation(str1, str2))
    
    def test_false_permutation_2(self):
        str1 = "aab"
        str2 = "bba"
        self.assertFalse(check_permutation(str1, str2))



unittest.main()