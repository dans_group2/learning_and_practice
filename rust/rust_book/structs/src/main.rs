#[derive(Debug)]
    struct User {
        active: bool,
        username: String,
        email: String,
        sign_in_count: u64,
    }


fn main() {
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    println!("{:?}", user1);

    let mut mutable_user = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    // mutate struct instance
    mutable_user.sign_in_count += 1;

    println!("{}", mutable_user.sign_in_count);

    let built_user = build_user(String::from("dan@dan.com"), String::from("daniel"));

    println!("{:?}", built_user);

    // merge instances of structs

    let merged_user = User {
        email: String::from("merged@email.com"),
        ..user1
    };

    println!("{:?}",merged_user);
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1
    }
}
