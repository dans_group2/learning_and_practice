use traits::{Summary, Tweet, NewsArticle};

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());

    let article = NewsArticle {
        headline: String::from("Penguins win the Staley Cup"),
        location: String::from("pittsburg"),
        author: String::from("Dan"),
        content: String::from("fjsdlkfjkldsfjlksdjflksdjflksd"),
    };

    println!("news article summary: {}", article.summarize())
}
