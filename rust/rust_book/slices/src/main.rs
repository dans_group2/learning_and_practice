fn main() {
    let mut s = String::from("hello my name is daniel");
    let first_word = get_first_word(&s);

    s.clear();

    print!("the first word is {}", first_word);
}

fn get_first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
