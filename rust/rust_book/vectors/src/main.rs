// fn main() {
//     let _v: Vec<i32> = Vec::new();

//     // or
//      let mut v = vec![1,2,3];

//      v.push(4);

//      println!("{:?}", v);
// }

// fn main() {
//     let v = vec![1,2,3];

//     let third: &i32 = &v[2];

//     println!("The third element is {}", third);

//     match v.get(2) {
//         Some(third) => {
//             println!("The third element is {}", third);
//         },
//         None => println!("There is no third element")
//     }
// }

fn main() {
    let v = vec![100, 32, 57];

    for i in &v {
        println!("{}", i);
    }

    let mut v = vec![100, 32, 57];

    for i in &mut v {
        // * is a derefence operator that allows us the mutate element in array
        *i += 50
    }

    println!("{:?}", v);
}
