fn largest<T: std::cmp::PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn largest_char(list: &[char]) -> char {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}


fn main() {
    let number_list = vec![34, 50, 100, 1];
    let result = largest(&number_list);
    println!("first largest: {}", result);

    let number_list = vec![1, 4, 3, 2];
    let result = largest(&number_list);
    println!("second largest: {}", result);
}