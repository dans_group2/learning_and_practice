fn returns_str() -> &'static str {
    let my_string = String::from("I am a string");
    "I am a string" // string literals are written into the rust binary and lives for the full lifetime of the app
}

fn main() {
    let my_str = returns_str();
    println!("{}", my_str);
}

struct Adventurer<'a> {
    name: &'a str,
    hit_points: u32,
}

impl Adventurer<'_> { // needs anonymous lifetime `'_'` because of str reference lifetime in struct defintion
    fn take_damage(&mut self) {
        self.hit_points -= 20;
        println!("{} has {} hp left!", self.name, self.hit_points);
    }
}