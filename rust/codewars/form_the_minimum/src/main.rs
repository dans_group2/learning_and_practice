use std::collections::HashSet;

fn main() {
    let printme = sort([3,2,1]);
    println!("{:?}", printme);
}

fn sort(input_array: [i32; 3]) -> [i32; 3] {
    let mut sorted = input_array.clone();
    for _ in 0..2 {
        for i in 0..2 {
            let current = sorted[i];
            let next = sorted[i + 1];

            if next <= current {
                sorted[i] = next;
                sorted[i + 1] = current;
            }
        }
    }
    return sorted;
}
