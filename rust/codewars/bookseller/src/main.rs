fn main() {
    println!("Hello, world!");
}

use std::collections::HashMap;

fn stock_list(list_art: Vec<&str>, list_cat: Vec<&str>) -> String {
    let stock_count_map = generate_stock_list_hashmap(list_art, &list_cat);
    let mut answer = String::new();
    for cat in list_cat {
        let count_as_string = stock_count_map.get(cat).unwrap().to_string();
        answer += &format!("({} : {}) - ", &cat, count_as_string)
    }
    return answer[..answer.chars().count() - 3].to_string();
}

fn get_stock_code(listing: &str) -> String {
    listing[..1].to_string()
}

fn get_stock_count(listing: &str) -> u32 {
    let listing_split = listing.split(" ");
    let listing_vec: Vec<&str> = listing_split.collect();

    listing_vec[1].parse::<u32>().unwrap()
}

fn create_empty_stock_hashmap(list_cat: &Vec<&str>) -> HashMap<String, u32> {
    let mut stock_count: HashMap<String, u32> = HashMap::new();

    for cat in list_cat {
        stock_count.insert(cat.to_string(), 0);
    }
    return stock_count;
}

fn generate_stock_list_hashmap(list_art: Vec<&str>, list_cat: &Vec<&str>) -> HashMap<String, u32> {
    let mut stock_count = create_empty_stock_hashmap(&list_cat);
    for listing in list_art {
        let code = get_stock_code(&listing);
        if stock_count.contains_key(&code) {
            let current_count = stock_count.get(&code).unwrap().clone();
            let new_count = get_stock_count(&listing);
            stock_count.insert(code, current_count + new_count);
        }
    }
    return stock_count;
}

#[cfg(test)]
mod tests {
    use super::*;

    fn dotest(list_art: Vec<&str>, list_cat: Vec<&str>, exp: &str) -> () {
        println!("list_art: {:?};", list_art);
        println!("list_cat: {:?};", list_cat);
        let ans = stock_list(list_art, list_cat);
        println!("actual:\n{:?};", ans);
        println!("expect:\n{:?};", exp);
        println!("{};", ans == exp);
        assert_eq!(ans, exp);
        println!("{};", "-");
    }

    #[test]
    fn basic_tests() {
        let mut b = vec!["BBAR 150", "CDXE 515", "BKWR 250", "BTSQ 890", "DRTY 600"];
        let mut c = vec!["A", "B", "C", "D"];
        dotest(b, c, "(A : 0) - (B : 1290) - (C : 515) - (D : 600)");

        b = vec!["ABAR 200", "CDXE 500", "BKWR 250", "BTSQ 890", "DRTY 600"];
        c = vec!["A", "B"];
        dotest(b, c, "(A : 200) - (B : 1140)");
    }
}
