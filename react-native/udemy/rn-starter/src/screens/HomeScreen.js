import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const HomeScreen = ({ navigation: { navigate } }) => {
  return (
    <View>
      <Text style={styles.text}>hi there</Text>
      <Button
        onPress={() => navigate("Components")}
        title="Go to components demo"
      />
      <Button title="Go to List Demo" onPress={() => navigate("List")} />
      <Button title="Go to Image demo" onPress={() => navigate("Image")} />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
});

export default HomeScreen;
