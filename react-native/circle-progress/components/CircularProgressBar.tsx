import {Animated, ColorValue, StyleSheet, View} from "react-native";
import React, {useEffect, useState} from "react";
import {HalfCircle} from "./HalfCircle";
import {CircularProgressBarProps} from "../types/CircularProgressBarProps";
import {HalfCircleProps} from "../types/HalfCircleProps";

export function CircularProgressBar(props: CircularProgressBarProps): JSX.Element {
    const baseColor = props.baseColor ? props.baseColor : 'gray'
    const progressColor = props.progressColor ? props.progressColor : 'powderblue'
    const size = props.size ? props.size : 100
    const strokeWidth = props.strokeWidth ? props.strokeWidth : 10;

    const {maxValue, currentValue} = props;

    // const [degrees, setDegrees] = useState(0)

    const [firstCicle, setFirstCircle] = useState<HalfCircleProps>()

    // useEffect(() => {
    //     setDegrees(currentValue * 360 / maxValue);
    // }, [])

    const styles = StyleSheet.create({
        baseCircle: {
            borderColor: baseColor,
            height: size,
            width: size,
            borderWidth: strokeWidth,
            borderRadius: size / 2,
            position: "absolute",
            transform: [
                { rotate: "-45deg" },
                // { rotateZ: "45deg" }
            ]
        }
    })

    function setDegrees() {
        first
    }

    return (
        <>
            <View style={styles.baseCircle}>
            </View>
            <HalfCircle
                size={size}
                strokeWidth={strokeWidth}
                progressColor={progressColor}
                baseColor={baseColor}
                degrees={degrees}
            />
            <HalfCircle
                size={size}
                strokeWidth={strokeWidth}
                progressColor={progressColor}
                baseColor={baseColor}
                degrees={degrees}
            />
            <HalfCircle
                size={size}
                strokeWidth={strokeWidth}
                progressColor={progressColor}
                baseColor={baseColor}
                degrees={degrees}
            />
        </>

    )


}