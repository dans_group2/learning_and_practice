import {ColorValue, StyleSheet, Text, View} from "react-native";
import React from "react";
import {HalfCircleProps} from "../types/HalfCircleProps";

export function HalfCircle(props: HalfCircleProps): JSX.Element {
    const {degrees, progressColor, strokeWidth, size, baseColor, borderBottomColor, borderLeftColor, borderRightColor, borderTopColor, rotate} = props;

    const styles = StyleSheet.create({
        halfCircle: {
            height: size,
            width: size,
            borderWidth: strokeWidth,
            borderRadius: size / 2,
            borderColor: progressColor,
            position: "relative",
            borderLeftColor: borderLeftColor,
            borderTopColor: borderTopColor,
            borderRightColor: borderRightColor,
            borderBottomColor: borderBottomColor,
            transform: [
                { rotate: degrees }, // 135 = zero progress
            ]
        },
        test: {

        }
    })

    return (

        <View style={[styles.halfCircle]}>

        </View>
    )
}