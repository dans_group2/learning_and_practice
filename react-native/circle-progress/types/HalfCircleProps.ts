import {ColorValue} from "react-native";

export type HalfCircleProps = {
    progressColor: ColorValue,
    size: number,
    degrees: number,
    strokeWidth: number,
    baseColor: ColorValue,
    borderLeftColor: ColorValue,
    borderTopColor: ColorValue,
    borderRightColor: ColorValue,
    borderBottomColor: ColorValue,
}
