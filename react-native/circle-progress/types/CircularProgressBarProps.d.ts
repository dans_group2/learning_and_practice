import {ColorValue} from "react-native";

export type CircularProgressBarProps = {
    maxValue: number,
    currentValue: number
    baseColor?: ColorValue;
    progressColor?: ColorValue;
    size?: number,
    strokeWidth?: number
}