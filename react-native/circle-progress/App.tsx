import {StyleSheet, View} from 'react-native';
import React from "react";
import {CircularProgressBar} from "./components/CircularProgressBar";

export default function App() {
    return (
        <View style={styles.container}>
            <CircularProgressBar maxValue={360} currentValue={1}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});




