import {StyleSheet, Text, View} from 'react-native';
import Animated, {useSharedValue, useAnimatedStyle, withTiming} from 'react-native-reanimated';
import {useEffect} from "react";

const SIZE = 100.0

export default function App() {
    // this value is handled by worklets on the UI thread
    const progress = useSharedValue(0);

    const reanimatedStyle = useAnimatedStyle(() => {
        return {
            opacity: progress.value
        }
    }, [])

    useEffect(() => {
        progress.value = withTiming(0, {duration: 5000});
    }, [])


    return (
        <View style={styles.container}>
            <Text>Hello</Text>
            <Animated.View style={[{height: SIZE, width: SIZE, backgroundColor: 'blue'}, reanimatedStyle]}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
