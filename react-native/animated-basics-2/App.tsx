import React, {useRef, useEffect, PropsWithChildren} from 'react';
import { Animated, Text, View } from 'react-native'

export default () : React.ReactNode => {
  return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <FadeInView style={{width: 250, height: 50, backgroundColor: 'powderblue'}}>
          <Text style={{fontSize: 28, textAlign: 'center', margin: 10}}>
            Fading In
          </Text>
        </FadeInView>
      </View>
  )
}

function FadeInView(props: PropsWithChildren<any>): React.ReactNode {
  // initial value for opacity: 0
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(
        fadeAnim,
        {
          useNativeDriver: true,
          toValue: 1,
          duration: 10000
        }
    ).start()
  }, [fadeAnim]);

  return(
      <Animated.View style={{ ...props.style, opacity: fadeAnim }}>
        {props.children}
      </Animated.View>
  )
}