import { Image } from "react-native"
import expoIcon from '../assets/adaptive-icon.png'

export default function LogoTitle() {
    return (<Image style={{width: 50, height: 50}} source={expoIcon} />)
}