import { StatusBar } from "expo-status-bar";
import { Alert, StyleSheet, Text, View  } from "react-native";
import Button from "./Button";
import { styleProps } from "react-native-web/dist/cjs/modules/forwardedProps";

export default function App() {
  function handleButtonPress() {
    Alert.alert("Alert", "You clicked this button!");
  }

  return (
    <View style={styles.container}>
      <Button title="My First Button" style={styles.button}>
        My First Button
      </Button>

      <Button success style={styles.button}>
        Success Button
      </Button>

      <Button info style={styles.button}>
        Info Button
      </Button>

      <Button danger rounded style={styles.button} onPress={handleButtonPress}>
        Rounded Button
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    margin: 10,
  }
});
