import React, { Component } from "react";

import { Text, TouchableOpacity } from "react-native";

import { Base, Default, Danger, Info, Success } from "./styles";

export default function Button({
  danger,
  info,
  success,
  rounded,
  children,
  onPress,
  style,
}) {
  function getTheme() {
    if (info) return Info;

    if (danger) return Danger;

    if (success) return Success;

    return Default;
  }

  const theme = getTheme()

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[Base.main, theme.main, rounded ? Base.rounded : null, style]}
      onPress={onPress}
    >
        <Text style={[Base.label, theme.label]}>
            {children}
        </Text>
    </TouchableOpacity>
  );
}
