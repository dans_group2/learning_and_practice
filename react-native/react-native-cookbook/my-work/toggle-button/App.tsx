import React, { useState } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native';

const heartIcon = require('./images/heart.png');

export default function App() {
  const [liked, setLiked] = useState<boolean>(false);

  function handleButtonPress() {
    setLiked(prev => !prev)
  }

  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={handleButtonPress} style={styles.button} underlayColor="#fefefe">
        <Image source={heartIcon} style={[styles.icon, (liked ? styles.liked : null)]} />

      </TouchableHighlight>
      <Text style={styles.text}>
        Do you like this app
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    alignItems: 'center'
  },
  button: {
    borderRadius: 5,
    padding: 10
  },
  icon: {
    width: 180,
    height: 180,
    tintColor: '#f1f1f1'
  },
  text: {
    marginTop: 20
  },
  liked: {
    tintColor: '#e74c3c'
  }
})