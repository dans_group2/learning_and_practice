class RomanToInteger
  def initialize
    @roman_map = {
      'I' => 1,
      'V' => 5,
      'X' => 10,
      'L' => 50,
      'C' => 100,
      'D' => 500,
      'M' => 1000
    }
  end

  # @param {String} s
  # @return {Integer}
  def roman_to_int(s)
    normal = normal_numeral(s)

    return s.chars.reduce(0) { |acc, char| acc + @roman_map[char] } if normal

    calculate_non_normal_numeral(s)
  end

  private

  def calculate_non_normal_numeral(s)
    sum = 0

    s = s.chars.reverse!
    s.slice(0, s.length - 1).each_with_index do |char, index|
      next_char = s[index + 1]
      sum -= @roman_map[next_char] * 2 if @roman_map[next_char] < @roman_map[char]
      sum += @roman_map[char]
    end

    sum
  end

  # returns booleant to indicate if normal roman number
  # normal roman number means no subtraction
  # normal roman number is when s is strictly decreasing or equal
  def normal_numeral(s)
    s = s.chars
    s.each_with_index do |char, index|
      next_index = index + 1

      next unless next_index < s.length
      return false if @roman_map[s[next_index]] > @roman_map[char]
    end

    true
  end
end

# def roman_to_int(s)

# end

pp RomanToInteger.new.roman_to_int('VIV')
