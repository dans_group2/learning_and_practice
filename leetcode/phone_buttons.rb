class Button
  attr_reader :name

  def initialize(name, letters_list = [])
    @name = name.to_s
    @letters = {@name => 1}

    letters_list.each_with_index do |item, index|
      @letters[item] = index + 1
    end
  end

  def button_count(letter)
    @letters[letter]
  end

  def includes_letter?(letter)
    @letters.include? letter
  end
end

class Keyboard
  def initialize(buttons = [
    Button.new(1, []),
    Button.new(2, %w[a b c]),
    Button.new(3, %w[d e f]),
    Button.new(4, %w[g h i]),
    Button.new(5, %w[j k l]),
    Button.new(6, %w[m n o]),
    Button.new(7, %w[p q r s]),
    Button.new(8, %w[t u v]),
    Button.new(9, %w[w x y z]),
    Button.new(0, [' '])
  ])
    @buttons = buttons

    @memo = {

    }

    @buttons.each_with_index { |button, index| @memo[button.name] = index }
  end

  def includes_letter?(letter)
    return true if @name == letter

    return true if @memo.include? letter

    @buttons.each_with_index do |button, index|
      if button.includes_letter? letter
        @memo[letter] = index
        return true
      end
    end
  end

  def get_button_for_letter(letter)
    return nil unless includes_letter? letter

    @buttons[@memo[letter]]
  end
end

def presses(phrase)
  keyboard = Keyboard.new

  phrase = phrase.downcase

  count = 0

  phrase.chars.each do |letter|
    if keyboard.includes_letter? letter
      button = keyboard.get_button_for_letter letter
      count += button.button_count(letter)
    end
  end

  count
end

pp presses("a02 ")
